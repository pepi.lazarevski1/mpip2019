package ukim.finki.mpip2019.db;

import android.content.Context;
import androidx.room.Room;
import ukim.finki.mpip2019.models.DzTrack;

import java.util.List;

public class DbProvider {

    private static DzDatabase database = null;

    public DbProvider(Context context) {
        if(database == null) {
            database = Room
                    .databaseBuilder(context, DzDatabase.class, "db-app")
                    .build();
        }
    }

    public List<DzTrack> getAllTracks() {
        return database.getDzTrackDao().getAll();
    }

}
